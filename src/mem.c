#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

static bool inited;

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void const* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t max(size_t val1, size_t val2) { return val1 > val2 ? val1 : val2; }

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

static struct region region_allocate_successfully(void* addr, size_t size, bool extends) { return (struct region) { .addr = addr,
                                                                                                                    .size = size,
                                                                                                                    .extends = extends}; }

extern inline bool region_is_invalid( const struct region* r );


static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*Alloc region*/
static struct region alloc_region ( void const* addr, size_t query ) {
  if (!addr) return REGION_INVALID;
  
  size_t neccessary_size = region_actual_size(query);

  if (map_pages(addr, neccessary_size, MAP_FIXED) != MAP_FAILED) {
    block_init(addr, size_from_capacity( (block_capacity) {neccessary_size}), NULL);
    return (struct region) { .addr = (void*) addr, neccessary_size, true };
  }
  else {
    void* new_rg_addr = map_pages(addr, neccessary_size, 0);
    if (new_rg_addr == MAP_FAILED) return REGION_INVALID;
    block_init(new_rg_addr, size_from_capacity( (block_capacity) {neccessary_size}), NULL);
    return region_allocate_successfully(new_rg_addr, neccessary_size, false);
  }
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  inited = true;
  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*Split block if too big*/

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block || !query || !block_splittable(block, query)) return false;

  size_t neccessary_capacity = max(BLOCK_MIN_CAPACITY, query);
  
  block_init(block->contents + neccessary_capacity, size_from_capacity((block_capacity) {block->capacity.bytes - neccessary_capacity}), NULL);
  block->next = (struct block_header*) ((int8_t*) block->contents + neccessary_capacity);
  block->next->capacity.bytes = block->capacity.bytes - size_from_capacity( (block_capacity) {neccessary_capacity} ).bytes;
  block->is_free = false;
  block->capacity.bytes = neccessary_capacity;

  return true;
}


/*Merging blocks*/

static void* block_after( struct block_header const* block ) {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous ( struct block_header const* fst,
                                struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (!block || !block->next || !mergeable(block, block->next)) return false;
  block->capacity.bytes = block->capacity.bytes + size_from_capacity(block->next->capacity).bytes;
  block->next = block->next->next;
  return true; 
}

static void merge_with_free_blocks(struct block_header* start) {
  while(try_merge_with_next(start));
}


struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header* block;
};

static struct block_search_result block_search_found_successfully(void* addr) { return (struct block_search_result) { .block = addr,
                                                                                                                      .type = BSR_FOUND_GOOD_BLOCK }; }

static struct block_search_result block_search_not_found(void* addr) { return (struct block_search_result) { .block = addr,
                                                                                                             .type = BSR_REACHED_END_NOT_FOUND}; }


static struct block_search_result find_good_or_last ( struct block_header* restrict block, size_t sz ) {
  if ( !block || !sz) return (struct block_search_result) { .block = NULL, .type = BSR_CORRUPTED };
  
  struct block_header* current = block;
  while (current) {
    merge_with_free_blocks(current);

    if (block_is_big_enough(sz, current) && current->is_free) {
      split_if_too_big(current, sz);
      return (struct block_search_result) { .block = current, .type = BSR_FOUND_GOOD_BLOCK };
    }

    if (current->next) current = current->next;
    else break;
  }

  return block_search_not_found(current);
}

/* Trying to find good block w/o growing */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) result.block->is_free = false;
  return result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (!last) return NULL;
  struct region new_region = alloc_region(block_after(last), query);
  if (region_is_invalid(&new_region)) return NULL;
  last->next = (struct block_header*) new_region.addr;
  return last->next;
}

static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (!heap_start) return NULL;
  struct block_search_result result = try_memalloc_existing(query, heap_start);
  if (result.type == BSR_REACHED_END_NOT_FOUND) {
    struct block_header* growed = grow_heap(result.block, query);
    result = try_memalloc_existing(query, heap_start);
  }
  return result.block;  
}

void* _malloc( size_t query ) {
  if (query <= 0) { fprintf(stderr, "Query must be greater that 0!"); return NULL; }
  if (!inited) heap_init(query);
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  merge_with_free_blocks(header);
}
