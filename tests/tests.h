#ifndef TESTS_H
#define TESTS_H

#include "../src/mem_internals.h"
#include "../src/mem.h"
#include "../src/util.h"
#include <sys/mman.h>


void test1();
void test2();
void test3();
void test4();
void test5();

void tests_run_all();

#endif