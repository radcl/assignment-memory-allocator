#include "tests.h"

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void test1() {
    void* first_block_contents = _malloc(23);
    void* second_block_contents = _malloc(23);
    void* third_block_contents = _malloc(512);
    struct block_header* first_block_header = block_get_header(first_block_contents); // heap_start
    struct block_header* second_block_header = block_get_header(second_block_contents);
    struct block_header* third_block_header = block_get_header(third_block_contents);
    
    //debug_heap(stdout, first_block_header);
    bool result = (first_block_header->capacity.bytes == 24)
        && (second_block_header->capacity.bytes == 24)
        && (third_block_header->capacity.bytes == 512);
    if (result) fprintf(stdout, "First test passed.\n");
    else fprintf(stderr, "First test failed.\n");

    _free(first_block_contents);    
    _free(second_block_contents);
    _free(third_block_contents);

    //debug_heap(stdout, first_block_header);
}

void test2() {
    void* fst_block_conts = _malloc(256);
    void* scnd_block_conts = _malloc(256);
    struct block_header* fst_block_header = block_get_header(fst_block_conts);
    struct block_header* scnd_block_header = block_get_header(scnd_block_header);
    //debug_heap(stdout, fst_block_header);

    _free(scnd_block_conts);

    //debug_heap(stdout, fst_block_header);
    bool result = fst_block_header->next->is_free;
    if (result) fprintf(stdout, "Second test passed.\n");
    else fprintf(stderr, "Second test failed.\n");

    _free(fst_block_conts);
    //debug_heap(stdout, fst_block_header);
}

void test3() {
    void* fst_block_conts = _malloc(8192-17);
    struct block_header* fst_block_header = block_get_header(fst_block_conts);
    void* scnd_block_conts = _malloc(1024);
    void* thrd_block_conts = _malloc(1024);
    struct block_header* scnd_block_header = block_get_header(scnd_block_conts);
    struct block_header* thrd_block_header = block_get_header(thrd_block_conts);
    //debug_heap(stdout, fst_block_header);

    _free(fst_block_conts);
    _free(scnd_block_conts);

    bool result = fst_block_header->is_free && scnd_block_header->is_free;
    if (result) fprintf(stdout, "Third test passed.\n");
    else fprintf(stderr, "Third test failed.\n");

    _free(thrd_block_conts);

    //debug_heap(stdout, fst_block_header);
}

void test4() {
    void* fst_block_conts = _malloc(1000);
    struct block_header* fst_block_header = block_get_header(fst_block_conts);
    //debug_heap(stdout, fst_block_header);
    void* scnd_block_conts = _malloc(64000);
    struct block_header* scnd_block_header = block_get_header(scnd_block_conts);
    //debug_heap(stdout, fst_block_header);
    bool result = (!fst_block_header->is_free) 
        && (fst_block_header->capacity.bytes == 1000)
        && (!scnd_block_header->is_free)
        && (scnd_block_header->capacity.bytes == 64000)
        && (scnd_block_header->next);

    if (result) fprintf(stdout, "Fourth test passed.\n");
    else fprintf(stderr, "Fourth test failed.\n");

    _free(fst_block_conts);
    _free(scnd_block_conts);
}

void test5() {
    void* fst_block_conts = _malloc(8000);
    struct block_header* fst_block_header = block_get_header(fst_block_conts);
    //debug_heap(stdout, fst_block_header);
    struct block_header* last_block = fst_block_header->next;
    void* res = mmap(last_block->capacity.bytes + last_block->contents, 512, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20, 0, 0);
    void* scnd_block_conts = _malloc(18000);
    struct block_header* scnd_block_header = block_get_header(scnd_block_conts);
    //debug_heap(stdout, fst_block_header);
    
    bool result = (fst_block_header->capacity.bytes == 8000) 
        && (fst_block_header->next->is_free)
        && (scnd_block_header->capacity.bytes == 18000);

    if (result) fprintf(stdout, "Fifth test passed.\n");
    else fprintf(stderr, "Fifth test failed.\n");
    
    _free(fst_block_conts);
    _free(scnd_block_conts);
}

void tests_run_all() {
    test1();
    test2();
    test3();
    test4();
    test5();
}